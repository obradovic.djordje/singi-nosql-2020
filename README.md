# singi-nosql-2020

Raspored tema po nedeljama nastave:

materijali: https://gitlab.com/obradovic.djordje/singi-nosql-2020

## 1) 2.3.2020.ponedeljak
- NoSQL baze - uvod i motivacija
- osobine i glavne teoreme
- modeli distribucije
- skaliranje i clould
- agregacije - map/reduce algoritam
- JSON
## 2) 9.3.2020. ponedeljak
- Key-value baze podataka

## 3) 16.3.2020. ponedeljak
- Dokument orijentisane baze

## 4) 23.3.2020. ponedeljak
- Kolonske baze

## 5) 30.3.2020. ponedeljak
- Graph orijentisane baze

## 6) 6.4.2020. ponedeljak
    - Kolokvijum 5 teorijskih pitanja + seminarski rad

## 7) 13.4.2020. ponedeljak
- transformacije podataka iz relacione baze u odgovarajucu NoSQL vrstu
     dokument/key-value/graph/column

## 8) 20.4.2020. ponedlejak neradni dan/USKRS
- demonstracija raznih modela distribucije 

## 9) 27.4.2020. ponedeljak
- BigData demonstracija obrade velike kolicine podataka 
Dokument orijentisani

## 10) 4.5.2020. ponedeljak
- BigData demonstracija obrade velike kolicine podataka 
Key/value

## 11) 11.5.2020. ponedeljak
- BigData demonstracija obrade velike kolicine podataka 
Graph/Column

## 12) 18.5.2020. ponedeljak
    - Kolokvijum II
    - ocena 6: 5 teorijskih pitanja + seminarski rad
    - ocena 8: implementacija jednog od cetri vrste 
    - ocena 10: implementacija hibridnog sistema (kombinacija vise vrsta)

## 13) 25.5.2020. ponedeljak
    Zavrsni projekti/ priprema

## 14) 1.6.2020. ponedeljak
    Zavrsni projekti/ priprema

## 15) 8.6.2020. ponedeljak
    Zavrsni projekti/ priprema
