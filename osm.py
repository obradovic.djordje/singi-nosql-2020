import xml.etree.ElementTree as ET

'''
https://wiki.openstreetmap.org/wiki/OSM_XML
https://download.geofabrik.de/europe/serbia-latest.osm.bz2

'''

filename = '/media/djordje/DATA01/serbia-latest.osm'


tag_keys = {}
tags = {}
cities = {}
streets = {}
count = 0
for event, elem in ET.iterparse(filename):
    # print('====================', event, elem.tag)
    # print(count, elem.tag, elem.attrib)
    city = None
    street = None
    for ee in elem:
        # print(ee.tag, ee.attrib)
        for atrelem in ee.attrib:
            # print(atrelem)
            for key in ee.attrib:
                if key == 'k':
                    key_value = ee.attrib[key]
                    if key_value not in tag_keys:
                        tag_keys[key_value] = 0
                    tag_keys[key_value] += 1

            if 'k' in ee.attrib and 'v' in ee.attrib:
                key = ee.attrib['k']
                value = ee.attrib['v']
                if key == 'addr:city':
                    city = value
                if key == 'addr:street':
                    street = value
    if city is not None and street is not None:
        if city not in cities:
            cities[city] = 0
        cities[city] += 1
        addr = city + '\t'+street
        if addr not in streets:
            streets[addr] = 0
        streets[addr] += 1

    if elem.tag not in tags:
        tags[elem.tag] = 0
    tags[elem.tag] += 1

    if count%100000 == 0:
        stag = [[k, v] for k, v in sorted(tag_keys.items(), key=lambda item: item[1], reverse=True)]
        print(count, tags)
        for el in stag[:15]:
            print(el)

        scities = [[k, v] for k, v in sorted(cities.items(), key=lambda item: item[1], reverse=True)]
        # print(count, scities)
        with open('cities.txt', 'w') as file:
            for el in scities:
                print(el[0], el[1], file=file)

        sstreets = [[k, v] for k, v in sorted(streets.items(), key=lambda item: item[1], reverse=True)]
        with open('streets.txt', 'w') as file:
            for el in sstreets:
                print('{}\t{}'.format(el[0], el[1]), file=file)

    if event == 'end' and elem.tag in ['node', 'way', 'relation']:
        elem.clear()
    count += 1
    if count>31110000:
        break